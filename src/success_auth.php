<?php
//Страница с успешной авторизацией и выводом текста для авториз. пользователя
session_start();
 if (isset($_SESSION['login'])){
     echo "<h2>" . 'Здравствуйте, ' . $_SESSION['login'] . "." . "</h2>";
 } else {
     header('Location: ../index.php');
 }
?>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu sapien vitae nisi eleifend convallis quis sit amet tellus. Fusce imperdiet, metus ac rutrum scelerisque, dolor ligula posuere sapien, ut iaculis urna odio non sem. Ut semper maximus nulla, eget ultricies purus fringilla quis. Maecenas nisl nisi, tincidunt id purus ac, convallis sollicitudin lacus. Phasellus et tincidunt tellus. Etiam mollis dolor nibh, quis mollis mi convallis quis. Praesent non leo rhoncus, vestibulum ante ut, sodales velit.</p>
<form method="POST" action="change_info.php">
    <button type="submit">Изменить профиль</button>
</form>

<form method="POST" action="destroy_session.php">
    <button type="submit">Выход</button>
</form>

</body>
</html>
