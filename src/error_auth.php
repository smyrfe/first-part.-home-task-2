<?php
//Вывод ошибок при не правильной авторизации
session_start();
    if (isset($_SESSION['invalidPassword'])){
        echo "<h2>". "Извините, введен неверный пароль для пользователя ". $_SESSION['login']. "</h2>";
    }
    elseif(isset($_SESSION['emptyValues'])){
        echo "<h2>". "Заполните все поля ввода" . "</h2>";
    }
    else {
        echo "<h2>". "Извините, пользователь ". $_SESSION['login']. " не зарегистрирован". " или не заполнены все поля ввода ". "</h2>";
    }?>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<form method="POST" action="destroy_session.php">
    <button type="submit">На главную</button>
</form>
</body>
</html>