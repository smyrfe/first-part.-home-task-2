<?php
session_start();
require '../accounts.php';

// Если
if (isset($_POST['login']) && isset($_POST['password'])) {
    if (isset($accounts[$_POST['login']]) && $accounts[$_POST['login']] == $_POST['password']){
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['password'] = $_POST['password'];
        header('Location: ../success_auth.php');
    } //Проверка на неправильный пароль для пользователя
     elseif (isset($accounts[$_POST['login']]) && $accounts[$_POST['login']] !== $_POST['password']) {
         $_SESSION['invalidPassword'] = 'Invalid password';
         $_SESSION['login'] = $_POST['login'];
         header('Location: ../error_auth.php');
     } //Если пользователь не зарегистрирован или не заполнены все поля
     else {
            $_SESSION['login'] = $_POST['login'];
            header('Location: ../error_auth.php');
     }
}
