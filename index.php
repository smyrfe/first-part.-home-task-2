<?php
session_start();
if (isset($_SESSION['login'])) {
    header('Location: /src/success_auth.php');
}
?>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<h1>Вход в аккаунт</h1>
<form method="POST" action="src/handlers/auth_handler.php">
    <label>Логин:</label>
    <input type="text" name="login">

    <label>Пароль:</label>
    <input type="password" name="password">

    <button type="submit">Войти</button>
</form>
</body>
</html>
